# Angular 6 with Angular Material 2

This is a demo project for Angular 6 with Angular Material 2.

## Development Build**

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## contact details 

phone - 7990782930

## Github


## Activities Done - 

I have covered below details -

* fetching data over http call
* sharing data over files
* sorting functionality
* searching functionality
* pagination
* theme changing by toggling more button in header
* version control
