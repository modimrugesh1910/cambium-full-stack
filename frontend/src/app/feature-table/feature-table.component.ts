import {Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import * as _ from 'lodash';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {AppService} from "../shared/app.service";

@Component({
    selector: 'app-feature-table',
    templateUrl: './feature-table.component.html',
    styleUrls: ['feature-table.component.scss'],
    preserveWhitespaces: false
})

export class FeatureTableComponent implements OnInit, AfterViewInit, OnDestroy {
    /** Collection of subscribed variables */
    subscriptions: Subscription[] = [];
    /** loanDetails container */
    loanDetails: MatTableDataSource<any>;
    /** display column header */
    displayedColumns = ['action', 'member_id', 'loan_amnt', 'funded_amnt_inv', 'term', 'int_rate', 'installment', 'grade', 'emp_title', 'emp_length', 'home_ownership', 'annual_inc', 'verification_status', 'issue_d', 'loan_status', 'desc', 'purpose', 'title', 'addr_state', 'last_pymnt_d', 'last_pymnt_amnt'];
    /** 404 error bool*/
    loanDetailsNotFound = false;
    /** view child containers */
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;
    clientWidth: number = 0;
    pageSizeOptions: number[] = [5, 10, 25, 100];
    selectedUser: any;
    selectedUserBool: boolean = false;
    top_verfied_customers: any;
    top_not_verfied_customers: any;

    constructor(private appService: AppService, private router: Router) {
    }

    ngOnInit() {
        this.clientWidth = window.innerWidth - 50;

        // listen to getting loanDetails
        this.appService.fetchLoanDetails().subscribe(
            (data: any) => {
                if (data.length === 0) {
                    this.loanDetailsNotFound = true;
                    return;
                }
                // clear up variable
                this.loanDetailsNotFound = false;
                this.loanDetails = new MatTableDataSource(data);
                this.loanDetails.paginator = this.paginator;
                this.loanDetails.sort = this.sort;

                // top 10 verified customers
                this.top_verfied_customers = _.sortBy(data, (dta) => {
                    return dta.loan_amnt;
                }).filter((data) => {
                    if (data.verification_status == 'Verified')
                        return data;
                }).slice(-10);

                this.top_verfied_customers = new MatTableDataSource(this.top_verfied_customers);

                // top 10 not verified customers
                this.top_not_verfied_customers = _.sortBy(data, (dta) => {
                    return dta.loan_amnt;
                }).filter((data) => {
                    if (data.verification_status == 'Not Verified')
                        return data;
                }).slice(-10);
                this.top_not_verfied_customers = new MatTableDataSource(this.top_not_verfied_customers);
            });
    }

    ngAfterViewInit() {
        // listen to getting loanDetails
        if (this.loanDetails !== undefined)
            this.appService.fetchLoanDetails().subscribe(
                (data: any) => {
                    if (data.length === 0) {
                        this.loanDetailsNotFound = true;
                        return;
                    }
                    // clear up variable
                    this.loanDetailsNotFound = false;
                    this.loanDetails = new MatTableDataSource(data);
                    this.loanDetails.paginator = this.paginator;
                    this.loanDetails.sort = this.sort;
                });
        // this will be used first time sorting and pagination
        if (this.appService.loanDetails.length !== 0) {
            this.loanDetails.paginator = this.paginator;
            this.loanDetails.sort = this.sort;
        }
    }

    /**
     * filter loanDetails
     * @param filterValue
     */
    applyFilter(filterValue: string) {
        this.loanDetails.filter = filterValue.trim().toLowerCase();

        if (this.loanDetails.paginator) {
            this.loanDetails.paginator.firstPage();
        }
    }

    getClickData(data) {
        this.selectedUser = data;
        this.selectedUserBool = true;
    }

    /* called when component is being destroyed
     * clean memory or unsubscribe to current events to avoid memory leaks */
    ngOnDestroy() {
        for (const subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }
}
