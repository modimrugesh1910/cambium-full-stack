// import built-in modules by angular and other third party libraries or frameworks
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

// import in-app constants
import {Subject} from "rxjs";
import {map} from "rxjs/operators";
import {URL_API} from "./app.constant";
import {MatSnackBar} from "@angular/material";
// import in-app interface
// import in-app provider
// import in-app functions

@Injectable()
export class AppService {
    loanDetails: Array<any> = [];
    constructor(private router: Router, private httpClient: HttpClient, private snackBar: MatSnackBar) {
    }

    /**
     * fetching loan data
     */
    fetchLoanDetails(): any {
        /* set up header parameters
         * include access token stored in localStorage */
        let headers: HttpHeaders = new HttpHeaders();

        return this.httpClient.get(URL_API.LoanLIST()).pipe(map((res: any) => {
                this.loanDetails = [];
                this.loanDetails = res;
                return res;
            },
            (err: HttpErrorResponse) => {
                // clear out array when we got 404 call
                this.loanDetails = [];
                /* if request is unauthorized or forbidden, clean localStorage (mainly to remove token and userId)
                 * and navigate to home to check authentication again */
                if (err.status.toString().match(/401|403/)) {
                    localStorage.clear();

                }
            }));
    }
}
