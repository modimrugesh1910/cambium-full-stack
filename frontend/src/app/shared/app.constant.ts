const EXPRESS_SERVER: string = 'http://localhost:3000/apis/';

/** URL strings used in API calls */
export const URL_API = {
    LoanLIST: function (): string {
        return (EXPRESS_SERVER + 'list-data');
    },
};
