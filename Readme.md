#Cambium Developer Hiring Challenge - Lending Club
----------
# Stack Used

Front-End - Angular
Back-End - NodeJS, ExpressJS
DB - MongoDB

* Git Details - 
https://bitbucket.org/modimrugesh1910/cambium-full-stack/src

* Note - Please follow all details are given in the front-end and back-end readme.md files.

# Level - 2 changes done -

* Feature to search customers by using the member ID. 
* Implement request rate limiting for the server
* Specific page with loan details of a particular user on the front end, I have implemented it in a below part of table.
* started with Create a widget of  top 10 customers, by loan_amnt and verification_status = verified and not verified, widget is created in bitbucket git due to time constrain.
* Comment & document your code
* handling exception
