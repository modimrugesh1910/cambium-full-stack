'use strict';
var MongoClient = require('mongodb');
var commonService = require('../../libs/CommonService/service');
var _ = require('lodash');
const RateLimiter = require('../../libs/rate-limit-config');

module.exports = class ClubController {
    constructor(app) {
        this.rateLimiter = new RateLimiter(app);
        // app.get('/apis/list-data', this.rateLimiter.usingRemoteAddress(), this.listLoanData);
        app.get('/apis/list-data', this.listLoanData);
    }

    /**
     * @method listLoanData
     * @description  Method to get the list of loan data
     * @param req
     * @param res
     */
    listLoanData(req, res) {
        MongoClient.connect("mongodb://localhost:27017/", function (err, db) {
            if (err) throw err;

            const dbo = db.db("cambium");
            dbo.collection("lending_club").find({}).toArray(function (err, data) {
                if (err) throw err;
                res.send(data);
            });
        });
    }
};