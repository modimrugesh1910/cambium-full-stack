// import * as _ from 'lodash';
var _ = require('lodash');
var messages = require('../messages/en.json');
// language:String = 'en';
module.exports = {
    getMessage(key) {
        return _.get(messages, key);
    },
};
