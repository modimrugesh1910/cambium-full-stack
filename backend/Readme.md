**Cambium FUll Stack API**
----

* To run apis Application
 
1. npm install
2. nodemon server.js or node server.js
 
** Root URL will be http://localhost:3000/admin-api

* Import data into local system 
mongoimport -d cambium -c lending_club --type csv --file loan_part_0009b606f.csv --headerline

here loan_part_0009b606f.csv is downloaded given csv file on hackerearth.

* export data
** for csv option
mongoexport --host localhost -d cambium -c lending_club --type csv --out loan_part.csv --fields member_id,loan_amnt,funded_amnt_inv,term,int_rate,installment,grade,emp_title,emp_length,home_ownership,annual_inc,verification_status,issue_d,loan_status,desc,purpose,title,addr_state,last_pymnt_d,last_pymnt_amnt

** for json option
mongoexport --host localhost -d cambium -c lending_club --type json --out loan_part.json --fields member_id,loan_amnt,funded_amnt_inv,term,int_rate,installment,grade,emp_title,emp_length,home_ownership,annual_inc,verification_status,issue_d,loan_status,desc,purpose,title,addr_state,last_pymnt_d,last_pymnt_amnt
 
# Feature - Request rate limiting for the server

* For this you need to install Redis and "express-limiter" npm package to limit request rate. 
 
# Redis installation

* sudo apt-get install redis-server
* enable  service - sudo systemctl enable redis-server.service 
* restart service - sudo systemctl restart redis-server.service

* If you want to put limit please add this.rateLimiter.usingRemoteAddress() as second paramer in index.js of club folder.
 
* rate limit - 25 requests you can change it by changing limit by changing property of total in rate-limit-config.js




